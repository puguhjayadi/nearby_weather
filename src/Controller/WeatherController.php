<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class WeatherController extends Controller
{
    public function index()
    {
        $number = random_int(0, 100);

        return $this->render(
            'index.html.twig',
            array('number' => $number)
        );
    }
}